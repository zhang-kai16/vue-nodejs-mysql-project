import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// 配置路由规则
const routes = [
  /* 
  一级路由:
    /login
    /home
    /* 
  */
  {
    path: '/login',
    name: 'login',
    component: () => {
      return import('@/views/Login')
    },
  },
  {
    path: '/home',
    name: 'home',
    redirect: '/home/dashboard', //重定向到图表页
    component: () => {
      return import('@/views/Home')
    },
    children: [
      /* 
      二级路由:
        /home/dashboard
        /home/user
        /home/article
        /home/category
        /home/comments
        /home/setting
      */
      {
        path: 'dashboard',
        component: () => {
          return import('@/views/Home/Dashboard')
        },
      },
      {
        path: 'user',
        component: () => {
          return import('@/views/Home/User')
        },
      },
      {
        path: 'article',
        component: () => {
          return import('@/views/Home/Article')
        },
      },
      {
        path: 'deal',
        component: () => {
          return import('@/views/Home/Deal')
        },
      },
      {
        path: 'article/add',
        component: () => {
          return import('@/views/Home/ArticleAdd')
        },
      },
      {
        path: 'article/update',
        component: () => {
          return import('@/views/Home/ArticleUpdate')
        },
      },
      {
        path: 'category',
        component: () => {
          return import('@/views/Home/Category')
        },
      },
      {
        path: 'message',
        component: () => {
          return import('@/views/Home/Message')
        },
      },
      {
        path: 'comment',
        component: () => {
          return import('@/views/Home/Comment')
        },
      },

      {
        path: 'setting',
        component: () => {
          return import('@/components/Test')
        },
        children: [
          /* 
          三级路由:
          /home/setting/set01
          /home/setting/set02
          */
          {
            path: 'set01',
            component: () => {
              return import('@/components/Test')
            },
          },
          {
            path: 'set02',
            component: () => {
              return import('@/components/Test')
            },
          },
        ],
      },
    ],
  },
  {
    path: '/',
    redirect: '/home/dashboard', //重定向到图表页
  },
  {
    path: '/*',
    name: 'notfoud',
    component: () => {
      return import('@/views/NotFound')
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

// 设置导航守卫
router.beforeEach((to, from, next) => {
  // let islogin = localStorage.getItem("isLogin");
  let token = localStorage.getItem('token')
  // request.headers['Authorization'] = `Bearer ${token}`;
  // if (islogin) {
  if (token) {
    // 已登录
    if (to.path == '/login') {
      next('/home/dashboard')
    } else {
      next()
    }
  } else {
    // 未登录
    if (to.path == '/login') {
      next()
    } else {
      next('/login')
    }
  }
})

export default router
