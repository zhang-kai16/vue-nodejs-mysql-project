// 导入node的核心模块
const path = require('path')

const resolve = (dir) => path.join(__dirname, '.', dir)
module.exports = {
  // devServer: {
  //   allowedHosts: [
  //     '.moumou.com', // 允许访问的域名地址
  //   ],
  // },
  transpileDependencies: true,
  chainWebpack: (config) => {
    // 配置路径别名
    config.resolve.alias
      .set('@', resolve('src'))
      .set('assets', resolve('./src/assets'))
      .set('images', resolve('./src/assets/images'))
      .set('styles', resolve('./src/assets/styles'))
      .set('font_family', resolve('./src/assets/font_family'))
  },
}
