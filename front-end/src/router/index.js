import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  //设置一级路由

  {
    path: '/',
    redirect: '/recommend', //重定向
  },
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView'),
    //设置二级路由
    children: [{
        path: 'myArticle',
        component: () => import('../views/HomeView/myArticle'),
      },
      {
        path: 'article',
        component: () => import('../views/HomeView/article'),
      },
      {
        path: 'category',
        component: () => import('../views/HomeView/category'),
      },
      {
        path: 'timeLine',
        component: () => import('../views/HomeView/timeLine'),
      },
      {
        path: 'message',
        component: () => import('../views/HomeView/message'),
      },
      {
        path: 'friendWeb',
        component: () => import('../views/HomeView/friendWeb'),
      },
      {
        path: 'about',
        component: () => import('../views/HomeView/about'),
        meta: {
          requireAuth: true
        }
      },
      {
        path: 'publishArtical',
        component: () => import('../views/HomeView/publishArtical'),
      },
      {
        path: 'recommend',
        component: () => import('../views/HomeView/recommend'),
      },

      {
        path: 'test',
        component: () => import('../views/HomeView/test.vue'),
      },

    ],
  },

  {
    path: '*',
    name: 'notfound',
    component: () => import('../views/NotFound.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

// 路由守卫

export default router