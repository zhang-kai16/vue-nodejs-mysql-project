import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 导入element-ui安装文件
import './plugins/element.js'
// 导入vue-axios插件安装文件
// import './plugins/vue_axios.js'
import './plugins/http.js'
// 导入富文本编辑器插件安装文件
import './plugins/vue_quill_editor'
// 导入弹幕插件安装文件
// import './plugins/vue-danmaku'
import $ from "jquery";




router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireAuth)) {  // 判断该路由是否需要登录权限
    if (!sessionStorage.getItem('token') && !localStorage.getItem('token')) {
      next({
        path: '/home',
        // query: { redirect: to.fullPath }  // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
      alert('您需要先进行登录才能使用此功能')
    } else {
      next();
    }
  } else {
    next();
  }
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
