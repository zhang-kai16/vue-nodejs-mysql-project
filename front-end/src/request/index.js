// 自己编写Vue的插件

// 1. 导入axios库
import axios from 'axios'
// 2. 创建axios实例
const instance = axios.create({
  // 1. baseURL
  baseURL: 'http://localhost:3000',
  // 2. 超时时间, 一般是1000-3000
  timeout: 1000,
})
// 3. 配置axios(设置拦截器)

// 给instance配置响应拦截器
instance.interceptors.response.use(
  function (response) {
    // 成功的回调
    return response.data
  },
  function (error) {
    //失败的回调  把错误抛出去
    return Promise.reject(error)
  }
)
// 请求拦截器，每次请求都会先执行这里的代码
instance.interceptors.request.use(
  (config) => {
    // 请求头携带sessionStorage里面的token
    // 如果有token, 就添加请求头, 作为用户, 否则没有, 视为游客 (未注册用户)
    if (localStorage.getItem('token')) {
      config.headers.Authorization = 'Bearer ' + localStorage.getItem('token')
    }
    return config
  },
  function (err) {
    // 对请求错误做些什么
  }
)

// 4. 导出插件对象
export default {
  //插件(对象),里面有个install方法,Vue调用这个方法
  install(Vue) {
    //将instance挂载到Vue原型对象上
    Vue.prototype.$http = instance
  },
}
