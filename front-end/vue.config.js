// 导入node的核心模块
const path = require('path')
//  核心代码
const webpack = require("webpack");
const resolve = (dir) => path.join(__dirname, '.', dir)
module.exports = {
  // devServer: {
  //   allowedHosts: [
  //     '.moumou.com', // 允许访问的域名地址
  //   ],
  // },
  transpileDependencies: true,
  chainWebpack: (config) => {
    // 配置路径别名
    config.resolve.alias
      .set('@', resolve('src'))
      .set('assets', resolve('./src/assets'))
      .set('images', resolve('./src/assets/images'))
      .set('styles', resolve('./src/assets/styles'))
  },

  //  核心代码
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "windows.jQuery": "jquery",
      }),
    ],
  },

}
