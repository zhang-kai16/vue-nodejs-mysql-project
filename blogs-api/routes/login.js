var express = require('express')
var router = express.Router()
// 导入自己封装的数据库操作模块
const exec = require('../db')
//导入调试的标记
const {
  debug
} = require('../config')

//在路由里面调用刚才写好的方法生成Token   router.js
const {
  createToken
} = require('../jwt')

// 后台管理员登录接口
router.post('/admin', async function (req, res, next) {
  const {
    username,
    password
  } = req.body
  // 创建sql语句
  /* 根据用户名和密码查询用户表中的管理员信息 */
  // users/
  let sql = `select * from user_tb where username="${username}" and password="${password}" and role_name="admin"`

  // 返回数据库操作结果
  try {
    let data = await exec(sql)
    if (data.length == 1) {
      const token = createToken({
        username
      })
      res.send({
        code: 0,
        msg: '管理员登录成功!',
        token,
      })
    } else {
      res.send({
        code: 1,
        msg: '管理员登录失败!',
        result: debug ? err : '',
      })
    }
  } catch (err) {
    res.send({
      code: 1,
      msg: '管理员登录失败!',
      result: debug ? err : '',
    })
  }
})

// 前台用户登录接口
router.post('/', async function (req, res, next) {
  const {
    username,
    password
  } = req.body
  // 创建sql语句
  /* 根据用户名和密码查询用户表中的管理员信息 */
  // users/
  let sql = `select * from user_tb where username="${username}" and password="${password}"`

  // 返回数据库操作结果
  try {
    let data = await exec(sql)
    if (data.length == 1) {
      const token = createToken({
        username
      })
      res.send({
        code: 0,
        msg: '用户登录成功!',
        token,
      })
    } else {
      res.send({
        code: 1,
        msg: '用户登录失败!',
        result: debug ? err : '',
      })
    }
  } catch (err) {
    res.send({
      code: 1,
      msg: '用户登录失败!',
      result: debug ? err : '',
    })
  }
})

// 取登录用户的信息 get, 有权限的 API 接口, 获
router.get('/getinfo', async function (req, res, next) {
  // console.log('req.user', req.user)
  if (req.user)
    try {
      let sql = `select id,avatar,email,username,nickname,signature,password from user_tb where username="${req.user.username}"`
      let data = (await exec(sql))[0]
      res.send({
        code: 0,
        msg: '获取登录用户信息成功！',
        result: data, // 要发送给客户端的用户信息
      })
    } catch (err) {
      res.send({
        code: 1,
        msg: '获取登录用户信息失败！',
      })
    }
  else {
    res.send({
      code: -1,
      msg: '用户尚未登录！',
    })
  }
})
// 取登录用户的信息 get, 有权限的 API 接口, 获
router.get('/getinfo/admin', async function (req, res, next) {
  try {
    let sql = `select avatar,email,nickname,wechat,signature from user_tb where role_name="admin"`
    let data = (await exec(sql))[0]
    res.send({
      code: 0,
      msg: '获取管理员信息成功！',
      result: data, // 要发送给客户端的用户信息
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '获取管理员信息失败！',
    })
  }
})

module.exports = router