var express = require('express')
var router = express.Router()

//导入数据库操作模块
var exec = require('../db')
//导入调试的标记
const { debug } = require('../config')
// 三. 编写路由规则
/**
 * 查询所有评论，以及每个评论对应的用户昵称，用户头像
 * GET /messages
 */
router.get('/userinfo', async (req, res) => {
  let article_id = req.query.article_id
  let sql_msg_userinfo = `SELECT comment_tb.*,user_tb.nickname,user_tb.avatar 
  FROM comment_tb LEFT JOIN user_tb ON comment_tb.user_id=user_tb.id 
  WHERE comment_tb.article_id=${article_id};`
  let sql_total = `SELECT count(*) as total
  FROM comment_tb LEFT JOIN user_tb ON comment_tb.user_id=user_tb.id 
  WHERE comment_tb.article_id=${article_id};`

  try {
    let { total } = (await exec(sql_total))[0]
    let msg_userinfo = await exec(sql_msg_userinfo)
    res.send({
      code: 0,
      msg: '获取成功',
      result: {
        total,
        msg_userinfo,
      },
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '获取失败',
      result: debug ? err : '',
    })
  }
})

/* GET /messages?pages=1&num=5 */
router.get('/', async (req, res) => {
  // 解析参数,对象解构
  const { pages, num } = req.query
  let sql
  if (pages && num) {
    // 分页的公式: 偏移量 = (当前页码数 - 1)*每页显示数
    let offset = (pages - 1) * num
    // 操作数据库获取总记录数
    sql = `select count(*) as total from  message_tb`
    let { total } = (await exec(sql))[0] // { total: 1 }

    // 操作数据库获取XX页的xx条留言数据
    sql = `select * from  message_tb order by id desc
          limit ${offset}, ${num}`
    try {
      // 正常的情况
      const result = await exec(sql)
      res.send({
        code: 0,
        msg: '获取成功',
        result: { result, total },
      })
    } catch (err) {
      // 异常的情况
      res.send({
        code: 100101,
        msg: '获取失败',
        result: debug ? err : '',
      })
    }
    return
  } else {
    sql = `select * from  message_tb`
  }
  try {
    // 正常的情况
    const result = await exec(sql)
    res.send({
      code: 0,
      msg: '获取成功',
      result,
    })
  } catch (err) {
    // 异常的情况
    res.send({
      code: 100101,
      msg: '获取失败',
      result: debug ? err : '',
    })
  }
})
/**
 * 根据id获取留言
 * GET /message/:id
 */
router.get('/:id', async (req, res) => {
  // 一. 解析id
  let { id } = req.params
  // 二. 操作数据库
  const sql = `select * from message_tb where id = ${id}`
  try {
    // 三. 返回结果
    var result = await exec(sql)
    if (result) {
      res.send({
        code: 0,
        message: '获取成功',
        result: result,
      })
    } else {
      res.status(404).send({
        code: 100102,
        message: 'id对应的数据不存在',
        result: '',
      })
    }
  } catch (err) {
    // 异常的情况
    res.send({
      code: 1,
      msg: '添加失败',
      result: debug ? err : '',
    })
  }
})
/**
 * 添加留言
 * POST /message {content: '554'}
 */
router.post('/', async (req, res) => {
  // 解析参数(可以做参数格式的校验)
  const { content } = req.body
  // 操作数据库

  const sql = `insert into message_tb (content) values ('${content}')`

  try {
    // 正常的情况
    await exec(sql)

    // 返回结果
    res.send({
      code: 0,
      msg: '添加成功',
      result: '',
    })
  } catch (err) {
    // 异常的情况
    res.send({
      code: 1,
      msg: '添加失败',
      result: debug ? err : '',
    })
    debug ? console.log(err) : 1
  }
})

/**
 * 删除todo
 * Delete /todos/:id
 */
router.delete('/:id', async (req, res) => {
  // 解析参数
  let { id } = req.params
  // 操作数据库
  let sql = `delete from message_tb where id=${id}`
  
  try {
    // 正常的情况
    await exec(sql)
    res.send({
      code: 0,
      msg: '删除成功',
      result: '',
    })
  } catch (err) {
    // 异常的情况
    res.send({
      code: 1,
      msg: '添加失败',
      result: debug ? err : '',
    })
  }
})
module.exports = router
