//评价模块

var express = require('express')
var router = express.Router()
// 导入自己封装的数据库操作模块
const exec = require('../db')
// 导入调试的标记
const {
    debug
} = require('../config')




router.get('/', async function (req, res) {
    // console.log('req.params', req.params) // /:id
    // console.log('req.query:', req.query) // /?id=1&name='小明'
    // 搜索多条数据时, 显示的字段:
    let select_fields =
        'user_id,mark,id'
    //声明sql语句变量
    let sql
    // 获取全部数据列表 GET /articles
    if (JSON.stringify(req.query) == '{}') {
        sql = `SELECT ${select_fields} FROM evaluate_tb`
    }

    // 数据响应
    try {
        res.send({
            code: 0,
            msg: '查询成功',
            result: await exec(sql),
        })
    } catch (err) {
        res.send({
            code: 1,
            msg: '失败',
            result: debug ? err : '',
        })
    }
})


router.post('/', async function (req, res, next) {

    // POST /comments/
    // 获取请求数据
    let body = req.body
    // 创建sql语句
    let sql = `insert into evaluate_tb (user_id,mark) 
    values ("${body.user_id}","${body.mark}")`
    // 返回数据库操作结果
    try {
        let data = await exec(sql)
        res.send({
            code: 0,
            msg: '添加成功!',
            result: data.insertId,
        })
    } catch (err) {
        res.send({
            code: 1,
            msg: '添加失败!',
            result: debug ? err : '',
        })
    }
})



// 导出路由对象
module.exports = router