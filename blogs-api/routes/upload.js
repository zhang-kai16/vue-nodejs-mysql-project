const baseURL = 'http://localhost:3000'
var express = require('express')
var router = express.Router()
// 导入自己封装的数据库操作模块
const exec = require('../db')
//导入调试的标记
const { debug } = require('../config')

// 引入multer模块,用来存储上传过来的文件流
const multer = require('multer')

//配置上传封面图片的存储信息
const storage_cover_picture = multer.diskStorage({
  //1.存储位置
  destination: function (req, file, callback) {
    // 参数解释 param1:错误信息  param2:上传图片的服务端保存路径，注意这里的路径写法
    callback(null, './public/cover_picture')
  },
  //2.文件名,在这儿采用了时间戳和图片文件原名为上传的图片文件命名，可以保证名字不重复
  filename: function (req, file, callback) {
    callback(null, `${Date.now()}-${file.originalname}`)
  },
})

//配置上传用户头像的存储信息
const storage_avatar = multer.diskStorage({
  //1.存储位置
  destination: function (req, file, callback) {
    // 参数解释 param1:错误信息  param2:上传图片的服务端保存路径，注意这里的路径写法
    callback(null, './public/avatar')
  },
  //2.文件名,在这儿采用了时间戳和图片文件原名为上传的图片文件命名，可以保证名字不重复
  filename: function (req, file, callback) {
    callback(null, `${Date.now()}-${file.originalname}`)
  },
})
// 创建上传封面图片的multer对象
const upload_cover_picture = multer({ storage: storage_cover_picture })
// 创建上传用户头像的multer对象
const upload_storage_avatar = multer({ storage: storage_avatar })

//设置封面图片上传路由
// upload.arr()的参数解释
// img 就是 el-upload 组件中的 name 名称(上传文件的名称,需要在前端el-upload组件的属性中设置好)
// 1 就是上传的文件个数
router.post(
  '/coverPicture',
  upload_cover_picture.array('coverPicture', 1),
  (req, res) => {
    // 获取前端上传的文件对象信息req.files
    let file = req.files
    // console.log(file[0])
    //====此时，图片已经保存至我们的服务端了====

    // 获取文件基本信息，封装好发送给前端
    let fileInfo = {}
    fileInfo.type = file[0].mimetype
    fileInfo.name = file[0].originalname
    fileInfo.size = file[0].size
    fileInfo.path = `${baseURL}/download/coverPicture/?img=` + file[0].filename
    res.send({
      code: 0,
      msg: '上传成功',
      result: fileInfo,
    })
  }
)

router.post('/avatar', upload_storage_avatar.array('avatar', 1), (req, res) => {
  // 获取前端上传的文件对象信息req.files
  let file = req.files
  // console.log(file[0])
  //====此时，图片已经保存至我们的服务端了====

  // 获取文件基本信息，封装好发送给前端
  let fileInfo = {}
  fileInfo.type = file[0].mimetype
  fileInfo.name = file[0].originalname
  fileInfo.size = file[0].size
  fileInfo.path = `${baseURL}/download/avatar/?img=` + file[0].filename
  res.send({
    code: 0,
    msg: '上传成功',
    result: fileInfo,
  })
})

// 导出路由对象
module.exports = router
