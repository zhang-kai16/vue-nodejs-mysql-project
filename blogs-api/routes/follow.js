var express = require('express')
var router = express.Router()

//导入数据库操作模块
var exec = require('../db')
//导入调试的标记
const {
  debug
} = require('../config')

// 查询
router.get('/', async function (req, res) {

  // 搜索多条数据时, 显示的字段:
  let select_fields =
    'id,follow_userId,followed_userId,follow_value'
  //声明sql语句变量
  let sql
  // 获取当前用户的关注列表
  if (req.query.follow_userId) {
    sql = `SELECT ${select_fields} FROM follow_tb where follow_userId=${req.query.follow_userId} `
  }

  //判断用户是否关注
  else if (req.query.follow_userId && req.query.followed_userId) {
    sql = `SELECT ${select_fields} FROM follow_tb where follow_userId=${req.query.follow_userId} && followed_userId=${req.query.followed_userId}`
  }
  // 数据响应
  try {
    res.send({
      code: 0,
      msg: '查询成功',
      result: await exec(sql),
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '失败',
      result: debug ? err : '',
    })
  }
})

// 新增关注用户 post /articles
router.post('/', async (req, res) => {
  let {
    follow_userId,
    followed_userId,
    follow_value
  } = req.body

  // 插入语句
  let sql = `INSERT INTO follow_tb (follow_userId,followed_userId,follow_value) VALUES ('${follow_userId}','${followed_userId}','${follow_value}')`

  try {
    //返回
    const data = await exec(sql)
    res.send({
      code: 0,
      msg: '添加成功',
      result: data.insertId
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '添加失败',
      result: debug ? err : ''
    })
  }
})

// 根据id修改博客内容 PUT /articles?id=2
router.put('/', async (req, res) => {


  // 获取请求数据
  let body = req.body
  // 过滤需要修改的数据
  let bodyfilter = ''
  for (let key in body) {
    if (body[key] !== '') {
      bodyfilter += `${key}='${body[key]}',`
    }
  }
  //删除句尾逗号
  bodyfilter = bodyfilter.slice(0, bodyfilter.length - 1)

  //修改内容
  let sql = `UPDATE job_tb SET ${bodyfilter} where id = ${req.query.id}`

  try {
    let data = await exec(sql)
    res.send({
      code: 0,
      msg: '修改成功',
      result: ''
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '修改失败',
      result: debug ? err : ''
    })
  }
})


// 软删除 DELETE /articles?id=1
router.delete('/:id', async (req, res) => {
  sql = `UPDATE follow_tb  where id = ${req.params.id}`

  //响应
  try {
    let data = await exec(sql)
    res.send({
      code: 0,
      msg: '删除成功',
      result: ''
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '删除失败',
      result: debug ? err : ''
    })
  }
})

module.exports = router